public class main {
    public static void main(String[] args){
        //Task 1
        System.out.println("Task 1 \n");

        customArrayList customArray = new customArrayList();

        //add value
        for(int i=0; i < 11; i++){
            customArray.add(i);
            System.out.println("Adding... " + customArray);
        }
         //add by index
        customArray.add(14,3);
        System.out.println("\nAdd by index: " + customArray);

        //remove by index
        customArray.removeByIndex(3);
        System.out.println( "\nRemove by index: " + customArray);

        //remove by value
        customArray.removeByValue(4);
        System.out.println( "\nRemove by value: " + customArray);


        //Task 2
        System.out.println("\nTask 2 \n");

        customLinkedList linkedList = new customLinkedList();
        linkedList.add(5);
        linkedList.add(6);
        linkedList.add(7);
        linkedList.add(8);
        linkedList.add(8);
        linkedList.add(10);
        linkedList.add(99);

        System.out.println("Add: " + linkedList);

        linkedList.add(16,3);
        linkedList.add(17,3);
        System.out.println("\nAdd by index: " + linkedList);

        linkedList.removeByIndex(4);
        System.out.println("\nRemove by index: " + linkedList);

        linkedList.removeByValue(8);
        System.out.println("\nRemove by value: " + linkedList);

        linkedList.replace(22,2);
        System.out.println("\nReplace: " + linkedList);

    }
}
