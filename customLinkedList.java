public class customLinkedList {
    private Node head;
    private int size;

    public customLinkedList() {
        Node node = new Node(0,null);
        this.head = node;
        this.size = 0;
    }

    public void add(int value){
        Node tail = getNodeByIndex(size - 1);
        Node node = new Node(value,null);
        tail.next = node;
        size++;
    }

    public void add(int value, int index){
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("Current size: " + size + ", index: " + index);
        }
        Node current = getNodeByIndex(index);
        Node next = current.next;
        Node node = new Node(value, next);
        current.next = node;
        size++;

    }

    private Node getNodeByIndex(int index) {
        if(index == -1)
            return head;
        Node current = head.next;
        for(int i = 0; i < index; i++){
            current = current.next;
        }
        return current;
    }

    public void removeByValue(int value){
        Node current = head.next;
        Node previous = head;

        while(current != null){
            if(current.value == value){
                Node next = previous.next.next;
                previous.next = next;
                size--;
            }
            previous = current;
            current = current.next;
        }
    }

    public void removeByIndex(int index){
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("Current size: " + size + ", index: " + index);
        }
        Node previous = getNodeByIndex(index - 1);
        Node next = previous.next.next;

        previous.next = next;
        size--;
    }
    public void replace(int value, int index){
    Node current = getNodeByIndex(index);

    current.value = value;

    }
    private class Node{
        private int value;
        private Node next;

        public Node(int value, Node next){
            this.value = value;
            this.next = next;
        }

    }

    @Override
    public String toString() {
        Node current = head.next;
        String res = "customLinkedList{" + "size=" + size + ", [" + current.value;
        current = current.next;

        while(current != null) {
            res += (", " + current.value);
            current = current.next;
        }

        return res + "]}";
    }
}