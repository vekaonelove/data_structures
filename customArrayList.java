import java.util.Arrays;

public class customArrayList {
    private int[] elements;
    private int currentIndex;

    private static final int DEFAULT_SIZE = 10;

    public customArrayList() {
        elements = new int[DEFAULT_SIZE];
    }

    public void add(int element) {
        if (currentIndex >= elements.length) {
            increaseArraySize();
        }
        elements[currentIndex++] = element;

    }

    public void add(int element, int index) {
        if (index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }
        if (currentIndex >= elements.length){
            increaseArraySize();
        }

        System.arraycopy(elements, index, elements, index + 1, currentIndex - index);
        currentIndex++;
        elements[index] = element;
    }

    private void increaseArraySize(){
        int newSize = currentIndex + 1;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }
    private void decreaseArraySize(){
        int newSize = elements.length - 1;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length - 1);
        elements = newElements;
    }
    public void removeByIndex(int index) {
        if (index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        } else if (index == elements.length - 1) {
            decreaseArraySize();
        } else {
            System.arraycopy(elements, index + 1, elements, index,  elements.length - index - 1);
            decreaseArraySize();
            if(index < currentIndex)
            {
                currentIndex--;
            }
        }
    }
    public void removeByValue(int element) {
        int index = -1;
        for (int i = 0;i < currentIndex; i++){
            if(elements[i] == element){
                index = i;
                break;
            }
        }

        if(index != -1){
            removeByIndex(index);
        }
    }

    public void replace(int element, int index) {
        if (index >= elements.length){
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }
        elements[index] = element;
    }

    @Override
    public String toString() {
        return "customArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }
}
